# board.py
# author: Andrew Baas

# This file contains the implementation of the Board class. It simulates the
# othello game.

# Imports from external dependencies
import numpy as np

# The Board class simulates the game of othello
class Board:

    # Initializes the board
    # rows: Number of rows in the game board
    # cols: Number of columns in the game board
    def __init__(self, rows = 6, cols = 6):
        
        # Set local variables
        self.rows = rows
        self.cols = cols
        self.state = []
        
        # Initialize the board ('e' means empty)
        for i in range(rows):
            self.state.append([])
            for j in range(cols):
                self.state[i].append('e')
        # Calculate the center and place the starting tiles
        midR = rows // 2
        midC = cols // 2
        self.state[midR][midC]       = 'w'
        self.state[midR][midC - 1]   = 'b'
        self.state[midR-1][midC-1]   = 'w'
        self.state[midR - 1][midC]   = 'b'
    
    # Prints the state of the board (primarily for debugging)
    def print_board(self):
    
        # Iterates through the board, placing '.' for empty spaces
        for c in range(self.cols):
            for r in range(self.rows):
                if self.state[r][c] == 'e':
                    print(".", end="")
                else:
                    print(self.state[r][c], end="")
            print("\n", end="")

    # Tests whether pieces will be converted in a certain direction, given an 
    # offset and whether to go ahead and convert pieces
    # x:    The x position to test from
    # y:    The y position to test from
    # xoff: The x offset to traverse
    # yoff: The y offset to traverse
    # color:   The color to test against
    # place:   Whether to update validly captured tiles
    # returns whether any pieces were captured
    def test_off(self, x, y, xoff, yoff, color, place=True):
        
        # Set the opposing color from the given color
        opp = 'w'
        if color == 'w':
            opp = 'b'

        # Ensure that we can validly traverse in this direction
        if x + xoff >= 0 and x + xoff < self.rows \
                         and y + yoff >= 0   \
                         and y + yoff < self.cols \
                         and self.state[x + xoff][y + yoff] == opp:
            dx = xoff
            dy = yoff
            # While we can, do the traversal
            while x + dx >= 0 and x + dx < self.rows \
                              and y + dy >= 0   \
                              and y + dy < self.cols \
                              and self.state[x + dx][y + dy] == opp:
                dx += xoff
                dy += yoff
            # If we hit a valid endpoint, then capture. Otherwise return False
            if x + dx >= 0 and x + dx < self.rows    \
                           and y + dy >= 0      \
                           and y + dy < self.cols    \
                           and self.state[x + dx][y + dy] == color:
                # Capture tiles if we're supposed to
                while place and (dx != 0 or dy != 0):
                    dx -= xoff
                    dy -= yoff
                    self.state[x + dx][y + dy] = color
                return True
        return False
        
    # This function places a tile on the board and captures pieces
    # loc: (x,y) coordinates to place a tile
    # color: The color of tile to place
    # place: Whether or not to actually place a tile
    # returns whether the location is a valid location
    def place_tile(self, loc, color, place=True):
        
        # Initialize position variables and find the opposing color
        x = loc[0]
        y = loc[1]
        opp = 'w'
        if color == 'w':
            opp = 'b'

        # First make sure the position is on the board
        if x >= self.rows or x < 0 or y < 0 or y >= self.cols:
            return False
        if self.state[x][y] != 'e':
            return False
        
        isValid = False
        # Determine if any directions are valid for capturing (and capture if
        # applicable)
        for xoff in range(-1,2):
            for yoff in range(-1,2):
                if self.test_off(x, y, xoff, yoff, color, place):
                    isValid = True

        return isValid

    # Determines and returns if a color has any valid moves
    def can_move(self, color):
        # Iterate through all locations until a valid move is found
        for r in range(self.rows):
            for c in range(self.cols):
                if self.place_tile((r, c), color, False):
                    return True
        return False

    # Generates and returns all valid moves for a given color
    def poss_moves(self, color):
        moves = [] 
        # Like above, only generate a list of valid locations instead of 
        # quitting at the first one
        for r in range(self.rows):
            for c in range(self.cols):
                if self.place_tile((r, c), color, False):
                    moves.append((r,c))
        return moves

    # Returns whether the game is not done
    def not_done(self):
        # Check each location to see if either color has a valid move
        for r in range(self.rows):
            for c in range(self.cols):
                if self.place_tile((r, c), 'w', False) or\
                   self.place_tile((r, c), 'b', False):
                    return True
        return False

    # Converts the board to a matrix of 1 for color, -1 for opposition color, 
    # and 0 for empty
    # color: The current player's color
    # returns the generated matrix
    def get_state(self, color):
        out = np.zeros((self.rows, self.cols))
        # Iterate through each location, performing the described conversion
        for r in range(self.rows):
            for c in range(self.cols):
                if self.state[r][c] != 'e':
                    if self.state[r][c] == color:
                        out[r,c] = 1
                    else:
                        out[r,c] = -1
        return out
   
    # Returns the score of the given color
    def get_score(self, color):
        score = 0
        for r in range(self.rows):
            for c in range(self.cols):
                if self.state[r][c] == color:
                    score += 1
        return score

    # Determines who won and by what margin
    # Returns a tuple with the winning color and the score margin
    def winning(self):
        b_count = 0
        w_count = 0
        for r in range(self.rows):
            for c in range(self.cols):
                if self.state[r][c] == 'b':
                    b_count += 1
                elif self.state[r][c] == 'w':
                    w_count += 1
        if b_count >= w_count:
            return ('b', b_count - w_count)
        return ('w', w_count - b_count)
