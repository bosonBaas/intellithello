# network.py
# author: Andrew Baas

# This file contains the implementation of the Network class.

# Imports from external dependencies
import math
import numpy as np

# The sigmoid activation function used in the neural network
def sigmoid(x):
  return 1 / (1 + math.exp(-x))

# The Neuron class is the basis. It simply has weights for each input and
# produces an output based on the activation function
class Neuron:
    
    # Initializes a Neuron object
    # weights: The initial weights for the Neruon inputs
    def __init__(self, weights):
        self.weights = weights

    # Sets the neuron weights from the given weights
    def set_weights(self, weights):
        self.weights = weights

    # Returns the neuron response to the given input array
    def activate(self, inputs):
        total = np.dot(self.weights, inputs)
        return sigmoid(total)

# The Layer class contains a 2-D array of Neurons. It can accept output from
# another layer and produces its own output
class Layer:

    # Initialize a Layer object
    # rows, cols: The number of rows and columns in the layer
    def __init__(self, rows, cols):
    
        # Set and initialize neurons in the layer
        self.neurons = []
        for r in range(rows):
            self.neurons.append([])
            for c in range(cols):
                # Seed the neurons with uniformly random weights
                self.neurons[r].append(Neuron(6 * np.random.random(9) - 3))
        self.rows = rows
        self.cols = cols

    # Processes input to the layer and returns output from it
    def proc_layer(self, i_arr):
        output = np.zeros((self.rows, self.cols))
        # Iterate through each neuron in the layer
        for r in range(self.rows):
            for c in range(self.cols):
                # Generate input to the neuron by checking each neighboring 
                # cell
                in_arr = np.zeros(9)
                for i in range(9):
                    # Check each neighboring cell of the input (including the
                    # one at the same spot)
                    dx = (i % 3) - 1
                    dy = (i // 3) - 1
                    if dx == 0 and dy == 0:
                        in_arr[i] = 1
                    if r + dx >= 0 and r + dx < self.rows\
                                   and c + dy >= 0       \
                                   and c + dy < self.cols:
                        in_arr[i] = i_arr[r + dx, c + dy]
                
                # Get the output from the neuron
                output[r,c] = self.neurons[r][c].activate(in_arr)
        return output

# The Network class manages multiple layers, and produces an overall output
class Network:

    # Initializes a Network object
    # rows, cols: Rows and columns in the network layers
    # depth:      Number of layers in the network
    def __init__(self, rows, cols, depth):
        
        # Set local variables and generate the layers
        self.depth = 0
        self.layers = []
        self.depth = depth
        for d in range(depth):
            self.layers.append(Layer(rows, cols))

    # Creates a list of moves based on a board state
    # board: The 1/0/-1 matrix format of an othello board
    # Returns a list of move choices sorted by order of preference
    def decide(self, board):
        # Process the board through the layers
        for l in self.layers:
            board = l.proc_layer(board)
            
        # Sort each location in the board by the greatest signal output
        ind = np.unravel_index(np.argsort(board, axis=None), board.shape)
        
        # Create a list of move choices
        choices = []
        for i in range(len(ind[0])):
            choices.append((ind[0][-1 - i], ind[1][-1 - i]))
        return choices
