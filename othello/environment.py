# environment.py
# author: Andrew Baas

# This file contains the implementation of the Environment class and utility 
# functions.

# Imports from external dependencies
import numpy as np

# Imports from local libraries
from othello.network import Network
from othello.board import Board

# This function is unused in the final program so far. It allows one to have
# the networks compete internally instead of againt random agents. I don't
# think it will currently work as-is
def challenge(net_a, net_b, soft=False):
   b = Board()

   cur_color = 'w'
   count = 0
   while b.not_done():
      if b.can_move(cur_color):
         if cur_color == 'w':
            moves = net_a.decide(b.get_state(cur_color))
            if soft:
               for loc in moves:
                  if b.place_tile(loc, cur_color, False):
                     break
            else:
               loc = moves[0]
         else:
            moves = net_b.decide(b.get_state(cur_color))
            if soft:
               for loc in moves:
                  if b.place_tile(loc, cur_color, False):
                     break
            else:
               loc = moves[0]
            
         is_valid = b.place_tile(loc, cur_color)
         if not is_valid:
            if cur_color == 'w':
               return ('b', count)
            return ('w', count)
         count += 1
      if cur_color == 'w':
         cur_color = 'b'
      else:
         cur_color = 'w'
   return (b.winning(), count)

# This function challenges a network against an agent who takes a random valid
# move
# color: The color of the network
# soft:  Whether to accept mutliple locations from the network)
def rand_challenge(net, color, soft=True):
   # Generate the board
   b = Board()

   # Initialize the game
   cur_color = 'w'
   count = 0
   loc = (0,0)
   
   # Play until there are no more valid moves
   while b.not_done():
      # Move if the color can, otherwise move to next player
      if b.can_move(cur_color):
         # Check who should play
         if cur_color == color:
            # If the network plays, it gets a state of the board and returns a
            # list of moves in order of preference. The first valid location is
            # accepted and used
            moves = net.decide(b.get_state(cur_color))
            if soft:
               for loc in moves:
                  if b.place_tile(loc, color, False):
                     break
            else:
               loc = moves[0]
         else:
            # The random agent makes a random selection from valid moves
            choices = b.poss_moves(cur_color)
            loc = choices[np.random.randint(0,len(choices))]
         
         # Place the tile if it can be placed. If soft=False, an invalid move
         # loses the game
         is_valid = b.place_tile(loc, cur_color)
         if not is_valid:
            if soft:
               print("Something went reeeeeal wrong...")
            if cur_color == 'w':
               return ('b', count)
            return ('w', count)
         # 'count' tracks the number of turns taken in a game
         count += 1
         
      # Swap colors
      if cur_color == 'w':
         cur_color = 'b'
      else:
         cur_color = 'w'
   # Return results
   return (b.winning(), count)

   
# The Environment class manages the evolutionary algorithm used to make fitter
# networks
class Environment:
   # Initializes an Environment object
   # pop_size: The size of the population to use
   # mut_rate: The rate of mutation of weights between generations
   # mem_size: The depth of the network
   def __init__(self, pop_size = 100, mut_rate = 0.01, mem_size = 10):
      # Initialize local variables
      self.pop_size = pop_size
      self.mut_rate = mut_rate
      self.mem_size = mem_size
      
      # This will store the population of networks
      self.population = []

      # Create an initial population of random networks
      for i in range(pop_size):
         self.population.append(Network(6, 6, mem_size))

   # This takes one generational step, scoring and breeding the current 
   # population
   # score_style: The algorithm used to score networks
   def generation(self, score_style="rand"):
      new_pop = []
      
      # Score the population using the given algorithm
      if score_style == "comp":
         score = self.score_comp()
      elif score_style == "rand":
         score, wins = self.score_rand()
      else:
         raise ValueError("invalid score style")

      # Total score calculated to show improvement between generations
      total = np.sum(score)
      
      # 50% of the best population randomly survives automatically
      survivors = int(self.pop_size * 0.5)
      # Sort population by score
      best = np.argsort(score)

      # Iterate through all empty spots in the next generation
      for n in range(self.pop_size - survivors):
         # Randomly choose a network net_a
         count = np.random.randint(0, total)
         net_a = 0
         for i in range(self.pop_size):
            if count < score[i]:
               net_a = i
               break
            count -= score[i]

         # Randomly choose a network net_b
         count = np.random.randint(0, total)
         count -= score[net_a]
         net_b = 0
         for i in range(self.pop_size):
            if i != net_a:
               if count <= score[i]:
                  net_b = i
                  break
               count -= score[i]
         
         # Breed the two networks
         new_pop.append(self.breed(self.population[net_a], self.population[net_b]))

      # Add the fittest networks from the old population to the new population
      for n in range(survivors):
         new_pop.append(self.population[best[n - survivors]])
      self.population = new_pop
      
      # Return the score of the best network from the old population
      return wins[best[-1]]

   # Iterates through all networks and scores them competitively
   # Not used in the final design
   def score_comp(self):
      score = np.ones(self.pop_size)
      turns = [] 
      for i in range(self.pop_size):
         for j in range(self.pop_size):
            if i != j:
               res, temp = challenge(self.population[i], self.population[j], soft=True)
               turns.append(temp)
               if res[0] == 'w':
                  score[i] += res[1]
               else:
                  score[j] += res[1]
      print(score)
      print(turns)
      print(max(turns))
      return score

   # Iterates through all networks and scores them against random agents
   def score_rand(self):
      # Define number of turns against random agents per network
      num_turns = 100
      
      # Store both the wins and the scores (we optimize for highest score)
      score = np.ones(self.pop_size)
      wins = np.zeros(self.pop_size)
      for i in range(self.pop_size):
         for j in range(num_turns):
            # Randomly choose the network's color
            color = 'w'
            if np.random.randint(0,2) == 0:
               color = 'b'
            # Obtain results from the challenge and add to the score
            res, temp = rand_challenge(self.population[i], color, soft=True)
            if res[0] == color:
               score[i] += res[1]
               wins[i] += 1 / (1+num_turns)
         # Wins stores a win percentage, not total number of wins. Need to add
         # 1 to keep from 0% (for log scale in graphing)
         wins[i] += 1/(1+num_turns)
      return score, wins

   # Breeds two given networks together by randomly taking different layers
   # and applying mutations
   # a, b: The two networks to breed
   def breed(self, a, b):
      # Create the final child object
      result = Network(6,6,self.mem_size)
      
      # Get the shape of the network weights
      w_shape = len(result.layers[0].neurons[0][0].weights)
      # Iterate through each layer
      for i in range(self.mem_size):
         # Iterate through each neuron
         for r in range(6):
            for c in range(6):
               # Randomly copy a neuron from one of the parents
               choice = np.random.randint(0,2)
               if choice == 1:
                  result.layers[i].neurons[r][c].weights = \
                       np.copy(a.layers[i].neurons[r][c].weights)
               else:
                  result.layers[i].neurons[r][c].weights = \
                       np.copy(b.layers[i].neurons[r][c].weights)
               
               # Apply a random mutation to this neuron at the mutation rate
               choice = np.random.random()
               if choice < self.mut_rate:
                  result.layers[i].neurons[r][c].weights += \
                      np.random.random(w_shape) * 1 - 0.5
      return result

   # This function was used for debugging, and allows you to compete against
   # the fittest network of the current population on the terminal
   def play_champion(self, color):
      b = Board()
      net = self.population[-1]
      cur_color = 'w'
      count = 0
      loc = (0,0)
      while b.not_done():
         b.print_board()
         if b.can_move(cur_color):
            print(cur_color + "'s turn:\n")
            if cur_color == color:
               moves = net.decide(b.get_state(cur_color))
               for loc in moves:
                  if b.place_tile(loc, color, False):
                     break
               else:
                  loc = moves[0]
            else:
               
               print("Input your move (r,c):")
               player_move = input().split()
               loc = (int(player_move[0]), int(player_move[1]))
               while not b.place_tile(loc, cur_color, False):
                  print("Invalid location, try again:")
                  player_move = input().split()
                  loc = (int(player_move[0]), int(player_move[1]))
                  
            is_valid = b.place_tile(loc, cur_color)
            if not is_valid:
               print("ERROR")
         else:
            print(cur_color + " has no valid moves.")
         if cur_color == 'w':
            cur_color = 'b'
         else:
            cur_color = 'w'
      return (b.winning(), count)
