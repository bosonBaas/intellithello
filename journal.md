# IntelliThello

## Todo list

[ ] Develop the intelligent software game
   [ ] Create trainable intelligent agent for the game
      [ ] Develop training with evolutionary algirithm
         [ ] Create algorithm
            [ ] Determine impact of # of layers
            [ ] Create a 2-input system
         [ ] Tune parameters
      [ ] Refine API and connection and flexibility
   [ ] Allow play against the agent
   [ ] Determine interactability with training the network
      [ ] Brainstorm for network interaction features
[ ] Integrate the game with the hardware
[ ] Create box for game
[ ] Test the game

## Journal

- The network I am making is restricted to communications with adjacent nodes on the next layer
   - This restriction will likely restrict learning ability (investigate this correlation)
- I tried to get the network to learn the rules
   - by self-play
      - No success. Only succeeded in learning how to make the first 3 moves in 1000 generations (maybe more could help)
   - by play with random agent
      - No success. Only succeeded in learning occasionally the first 3 moves (many couldn't get the first move...
   - Primary issue is that it is difficult to come up with a meaningful metric for if a network is "close" to the rules
- I then decided to let the board choose the network's highest preference valid move
   - This works much better
   - Still is slow to learn
   - Currently training it based on how many points it scores higher than a random agent (after a long time it should get decently good...)
- Maybe we can test with the netwok having 2 inputs (one for white and one for black) instead of -/+
