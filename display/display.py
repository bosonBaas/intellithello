# display.py
# author: Andrew Baas

# This file contains the implementation of the Display class. It acts as a 
# window manager and interacts directly with the OLED display API (oled)

# Imports from external dependencies
import numpy as np
from PIL import ImageFont
from time import sleep
from time import time

# Imports for OLED API
from oled.serial import i2c
from oled.device import ssd1306
from oled.render import canvas

# Imports from custom local libraries
from interact.rotary import Rotary
from display.game import Game
from display.menu import Menu
from display.train import Train

# The Display class manages the GUI. It contains the primary render loop and
# also manages hardware input. It wraps hardware input for individual pages to
# process.
class Display():

   # Font file location (We only use 1 font at this point)
   font = "/home/pi/Documents/intellithello/display/font/custom_font.otf"
   
   # Various sizes of font (appropriate for different locations)
   font_menu = ImageFont.truetype(font, size=8)     # Font for menus
   font_bg_game = ImageFont.truetype(font, size=16) # Font for scoring in game
   font_sm_game = ImageFont.truetype(font, size=8)  # Font for game notifications
   
   # Specifications for the root menu page
   items = ["Play", "Train"]              # Menu items
   options = [["1V1","AI b", "AI w"],[]]  # Menu options (1 array per item)
   
   # Seconds before the screen shuts off
   timeout = 300

   # Display object constructor
   # dis_port:     Port for oled I2C communication
   # dis_address:  I2C address of oled display
   # rot_y:        sw, dt, and clk gpio pins for the y-coordinate input
   # rot_x:        sw, dt, and clk gpio pins for the x-coordinate input
   def __init__(self, dis_port=1, dis_address=0x3c, rot_y = [11,12,13], rot_x = [29,31,32]):
      
      # Initialize the display (we only use 'device' to get the draw object later)
      serial = i2c(port=dis_port, address=dis_address)
      self.device = ssd1306(serial)
      
      # Initialize the hardware input
      self.y_rot  = Rotary(rot_y[0], rot_y[1], rot_y[2])
      self.x_rot  = Rotary(rot_x[0], rot_x[1], rot_x[2])

      # Set the stats for sleeping
      self.last_move = time()
      self.sleeping = False

   # Manages the input packaging and calling the rendering function of the
   # current page (creates an unending loop)
   def render(self):
      
      # Initializes subprocesses to listen to the rotary encoders
      self.y_rot.listen()
      self.x_rot.listen()
      
      # Initialize the input rotation counters
      prev_x = self.x_rot.counter
      prev_y = self.y_rot.counter
      
      # Create the main menu and set the current page
      page = Menu(self.items, self.options, self.font_menu)
      cur_page = "menu"

      # Enter the rendering loop
      while True:
         # Enter the drawing area
         with canvas(self.device) as draw:
            
            # Package input to pass to rendering function
            control = {}   # The object to store packaged input
            
            # Send rotary input based on rotation difference from last check
            # with a maximum change of 1
            cur_x = self.x_rot.counter
            cur_y = self.y_rot.counter
            if (cur_x - prev_x) == 0:
               control['x'] = 0
            elif cur_x - prev_x < 0:
               control['x'] = -1
            else:
               control['x'] = 1

            if (cur_y - prev_y) == 0:
               control['y'] = 0
            elif cur_y - prev_y < 0:
               control['y'] = 1
            else:
               control['y'] = -1
               
            # Process clicks from the rotary encoders (note the need to reset
            # the 'clicked' state each time
            control['x_clicked'] = self.x_rot.clicked
            if control['x_clicked']:
               self.x_rot.reset_click()
            
            control['y_clicked'] = self.y_rot.clicked
            if control['y_clicked']:
               self.y_rot.reset_click()

            # Check for any kind of movement to update the sleep time
            if control['x'] != 0 or control['y'] != 0 or control['y_clicked'] or control['x_clicked']:
               self.last_move = time()
               # Make sure that any clicks to wake up are not processed
               if self.sleeping:
                  control['x'] = 0
                  control['y'] = 0
                  control['y_clicked'] = False
                  control['x_clicked'] = False
                  self.sleeping = False

            # Actually render the applicable page and get the status (integer)
            # from the page
            status = page.render(draw, control)
            
            # Process the status
            if status != None:
               # Check if the menu is being displayed
               if cur_page == "menu":
                  # status == 0 means that 'Game' was selected
                  # status == 1 means that 'Train' was selected
                  if status == 0:
                     # Process the option (see 'self.options' to understand
                     # indices)
                     choice = page.get_options()[0]
                     ai = False
                     ai_color = 'w'
                     
                     if choice == 0:
                        ai = False
                     elif choice == 1:
                        ai = True
                        ai_color = 'b'
                     elif choice == 2:
                        ai = True
                        ai_color = 'w'
                     
                     # Create a new game with the selected options and make it 
                     # the current page
                     page = Game(self.font_sm_game, \
                                 self.font_bg_game, \
                                 ai, ai_color)
                     cur_page = "game"
                  
                  elif status == 1:
                     # Create the training page and make it the current page
                     page = Train(self.font_menu)
                     cur_page = "train"
               else:
                  # For non-menu, status == -1 means to return to the main menu
                  if status == -1:
                     # Create the main menu and make it the current page
                     page = Menu(self.items, self.options, self.font_menu)
                     cur_page = "menu"

            # Go ahead and update the x and y counters
            prev_y = cur_y
            prev_x = cur_x
            
            # If not training and the sleep time is exceeded, blank the screen
            if cur_page != "train" and time() - self.last_move > self.timeout:
               draw.rectangle((0,0,127,63),fill="black")
               self.sleeping = True
            
            # This makes no difference in the Raspberry Pi, but makes sure 
            # faster systems are throttled (not necessary for program functionality)
            sleep(0.01)