# game.py
# author: Andrew Baas
# This file contains the implementation of the Game class. This manages the
# game board, user interaction with it, and the display of that board

# Imports from external dependencies
import numpy as np

# Imports from custom local libraries
from display.util import draw_bitmap

# The Menu class is a general purpose menu which supports scrolling, as well
# as both item and option selection
class Menu():

   # General menu settings for text placement
   menu_size   = (0, 16, 127, 63)
   margin = 5

   # Initializes the Menu object
   # items:    Items in the menu list
   # options:  Options per-item (selected using the left rotation input)
   # font:     Font object for displayed text
   def __init__(self, items, options, font):
   
      # Initialize local variables
      self.items = items
      self.options = options
      self.font = font
      
      self.options_sel = len(options) * [0]   # Indices of option selections
      self.cur_selection = 0                  # Index of item selection
      
      # Defining offsets. The 'desired_offset' is the destination offset,
      # whereas the 'offset' is the actual offset. This is used for the
      # scrolling animation, as the movement each time is simply a function
      # of the difference between these two
      self.offset = self.get_desired_offset() # Current actual offset
      self.desired_offset = self.offset       # Desired offset (same to start)
      self.selected = None # Stores the index of a selection (if one is made)
      
      # The scroll functionality will scroll through the text of an item if the
      # item text is too long for the screen
      self.scroll = 0      # Stores the scroll offset

   # Returns the amount the current offset should be changed for the animation
   def smooth_scale(self):
      diff = self.desired_offset - self.offset
      if diff == 0:
         return 0
      # We have to calculate the sign to keep from imaginary values
      sign = diff / abs(diff)
      return sign * round(abs(2*diff) ** (1/2))

   # Gets the menu option selections
   def get_options(self):
      return self.options_sel

   # Changes which item is selected
   # selection: index of item to select
   def set_selection(self, selection):
      self.cur_selection = selection
   
   # Caclulates the proper offset to have the selected item centered
   def get_desired_offset(self):
      offset = self.menu_size[1] + (self.menu_size[3] - self.menu_size[1]) // 2
      for i in range(self.cur_selection):
         offset -= self.font.getsize(self.items[i])[1]
         offset -= self.margin
      offset -= self.font.getsize(self.items[self.cur_selection])[1] // 2
      return int(offset)

   # Updates the menu from user input and draws it
   # draw:     The draw image object used to draw on the display
   # control:  The hardware input
   def render(self, draw, control):
      self.selected = None
      self.update_state(control)
      self.draw_menu(draw)
      return self.selected

   # Draws the menu on the display
   # draw: The draw image object used to draw on the display
   def draw_menu(self, draw):
      cur_y = self.offset
      
      # Draw the text for each item
      for i in range(len(self.items)):
         
         # Retrieve the base text
         text = self.items[i]
         
         # If there are options, include them
         if len(self.options[i]) != 0:
            text += ": " + self.options[i][self.options_sel[i]]
         
         # Calculate the proper position for the text
         size = self.font.getsize(text)
         cur_x = (self.menu_size[2] - self.menu_size[0]) - size[0]
         cur_x //= 2
         
         # Draw a box around the item if it's the current selection
         if self.cur_selection == i:
            # Scrolls the text if it's too long for the display
            if size[0] + 6 > self.menu_size[2]:
               cur_x = 3 - self.scroll
            draw.rectangle([(cur_x - 3, cur_y - 3), \
                            (cur_x + size[0] + 3, cur_y + size[1] + 3)], \
                            width=2,outline="white")
         # Actually draws the text
         draw.text((cur_x, cur_y), text, font=self.font, fill="white", \ 
                     stroke_fill="white")
         cur_y += size[1] + self.margin
         
         # Covers any text that may be on the yellow portion of the screen
         draw.rectangle([(0,0),(self.menu_size[2], self.menu_size[1]-1)], \
                        fill="black")
      
      # Draw the selected text on the yellow title area
      # Same code as used for rendering it on the menu itself
      ind = self.cur_selection
      text = self.items[self.cur_selection]
      if len(self.options[ind]) != 0:
         text += ": " + self.options[ind][self.options_sel[ind]]
      top_size = self.font.getsize(text)
      top_x = (self.menu_size[2] - top_size[0]) // 2
      
      # Update the scroll for text that is too long for the display
      if top_size[0] + 6 > self.menu_size[2]:
         top_x = 3 - self.scroll
         self.scroll += 2
         self.scroll %= top_size[0] + 6 - self.menu_size[2]
      draw.text((top_x,0),text, font = self.font, fill="white")

   # Updates the menu state based on the input
   # control: the user input
   def update_state(self, control):
      
      # Choose an item if the y rotary is clicked
      if control['y_clicked']:
         self.selected = self.cur_selection
     
      # Scroll through selections with the y rotary
      if control['y'] != 0:
         self.cur_selection += control['y']
         self.cur_selection %= len(self.items)
         self.scroll = 0
         self.desired_offset = self.get_desired_offset()
      
      # Loop through options with x rotary if there are any options
      if len(self.options[self.cur_selection]) != 0:
         self.options_sel[self.cur_selection] += control['x']
         self.options_sel[self.cur_selection] %= len(self.options[self.cur_selection])
      
      # Update the current offset
      self.offset += self.smooth_scale()
