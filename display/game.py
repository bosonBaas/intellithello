# game.py
# author: Andrew Baas
# This file contains the implementation of the Game class. This manages the
# game board, user interaction with it, and the display of that board

# Imports from external dependencies
import numpy as np
import pickle  # Watch out, pickle files are loaded unsafely onto the system

# Imports from custom local libraries
from display.util import draw_bitmap
from display.util import get_brains
from othello.board import Board
from display.menu import Menu

# The Game class manages games between 2 players or between a player and a
# "brain"
class Game():
  
   # Bitmap arrays to display the tokens (1 is on, 0 is off)  
   black_token = [0x0E, 0x11, 0x11, 0x11, 0x0E]
   white_token = [0x0E, 0x1F, 0x1F, 0x1F, 0x0E]
   big_black_token = [0x1f8, 0x30c, 0x606, 0xc03, 0x801, 0x801, \
                      0x801, 0x801, 0xc03, 0x606, 0x30c, 0x1f8] 
   big_white_token = [0x1f8, 0x3fc, 0x7fe, 0xfff, 0xfff, 0xfff, \
                      0xfff, 0xfff, 0xfff, 0x7fe, 0x3fc, 0x1f8]
   
   # Dimensions of drawn items for rendering calculations
   token_size  = 5
   board_dim   = (40, 16, 87, 63)
   screen_dim  = (0,0,127,63)

   # Settings for the options menu
   menu_items   = ["Return","Reset", "Quit"] # Menu items
   menu_options = [[],[],[]]                 # Menu options (we don't have any)

   # Initializes the Game object
   # sm_font:  Font object to use for small labels
   # bg_font:  Font object to use for point labels
   # ai:       Whether an AI object will be playing
   # ai_color: Side the AI is playing on
   def __init__(self, sm_font, bg_font, ai=False, ai_color = 'w'):
      
      # Initialize fonts from arguments
      self.sm_font = sm_font
      self.bg_font = bg_font
      
      # Initialize board state variables
      self.select_pos = [4,4]    # Current selected position
      self.turn = 'w'            # Current player turn
      self.status = None         # The current return status
      self.menu_select = False   # Whether the menu has been selected
      self.done = False          # Whether the game is done
      self.results = ('w',-1)    # The results of the game
      self.board = Board()       # The game board
      self.replay_select = True  # Whether "Yes" is selected for 'Play again?'
      self.win_state = True      # Whether the game has been won
      self.ai = ai               # Whether AI should be used
      self.ai_color = ai_color   # Color for the AI
      self.brain = None          # Brain of the AI
      
      # Initialize the options menu
      self.menu = Menu(self.menu_items, self.menu_options, sm_font)
      
      # Set up the AI select menu
      self.ai_items = get_brains()              # Gets all files in "./brains/"
      self.ai_opts  = len(self.ai_items) * [[]] # We don't have any options
      self.ai_menu = Menu(self.ai_items, self.ai_opts, sm_font)

   # Renders the game board and updates the current state based off of input
   # draw:     The draw image object used to draw on the display
   # control:  The hardware input
   def render(self, draw, control):
      
      # Reset the current status
      self.status = None
      
      # Make sure AI is set up if AI is selected
      if self.ai and self.brain == None:
         # Render the ai select menu
         status = self.ai_menu.render(draw, control)
         
         # Check if brain is selected
         if status != None:
            # Load in menu with pickle !!!INSECURE!!!
            f_name = "brains/" + self.ai_items[status]
            with open(f_name, "rb") as b_file:
               brains = pickle.load(b_file)
               self.brain = brains[-1]
         # Break out to avoid rendering the board
         return self.status 
      
      # Check if the options menu has been selected
      if self.menu_select:
         # Render the options menu
         men_stat = self.menu.render(draw, control)
         
         # Check for each item being selected from menu
         if men_stat == 0:
            # Return to game
            self.menu_select = False
         elif men_stat == 1:
            # Reset the game
            self.reset()
         elif men_stat == 2:
            # Return to the main menu
            self.status = -1
      else:
         # Update board state and render the board
         self.update_state(control)
         self.draw_board(draw)
         
         # Draw the selected location with an extra token
         if not self.done:
            self.draw_select(draw)
            
      return self.status

   # Draws the current state of the board
   # draw: The draw object to draw on the display
   def draw_board(self, draw):
   
      # Get the board state (where tokens are) for rendering
      state = self.board.state
      
      # Initialize the score counters
      w_score = 0
      b_score = 0
      
      # Draw the board outline
      draw.rectangle(self.board_dim, outline="white", fill="black", width=2)
      
      # Draw each token
      for r in range(len(state)):
         for c in range(len(state[r])):
            # Add the score, and draw the appropriately colored token
            if state[c][r] == 'b':
               b_score += 1
               x = (self.token_size + 2) * c + self.board_dim[0] + 4
               y = (self.token_size + 2) * r + self.board_dim[1] + 4
               draw_bitmap(draw, x, y, self.black_token)
            elif state[c][r] == 'w':
               w_score += 1
               x = (self.token_size + 2) * c + self.board_dim[0] + 4
               y = (self.token_size + 2) * r + self.board_dim[1] + 4
               draw_bitmap(draw, x, y, self.white_token)
      
      # Draw the white score on the left
      w_size = self.bg_font.getsize(str(w_score))
      w_x = (self.board_dim[0] - 1 - w_size[0]) // 2
      w_y = (self.board_dim[3] - w_size[1])
      draw.text((w_x,w_y),str(w_score), fill="white", font = self.bg_font)
      
      # Draw the black score on the right
      b_size = self.bg_font.getsize(str(b_score))
      b_x = (self.board_dim[0] - 1 - b_size[0]) // 2 + self.board_dim[2] + 1
      b_y = (self.board_dim[3] - b_size[1])
      draw.text((b_x,b_y),str(b_score), fill="white", font = self.bg_font)

      # Draw the large black and white tokens on respective side
      draw_bitmap(draw, 14, self.board_dim[1] + 5, self.big_white_token)
      draw_bitmap(draw, 15 + self.board_dim[2], self.board_dim[1] + 5, \
                  self.big_black_token)

      # Draw the title-text (message in yellow zone)
      # This either shows who's turn it is, states who won, or allows user to choose to play again
      text = ""
      
      # Check if game is done
      if self.done:
         # Check if we're still showing who won
         if self.win_state:
            # Set title to who won
            if self.results[1] == 0:
               text = "Tie!"
            else:
               if self.results[0] == 'w':
                  text = "White Wins!"
               else:
                  text = "Black Wins!"
         else:
            # Set title to replay question and selection
            text = "Play Again: "
            if self.replay_select:
               text += "Yes"
            else:
               text += "No"
      else:
         # Set title-text to current turn
         if self.turn == 'w':
            text = "White Turn"
         else:
            text = "Black Turn"
            
      # Draw the title-text centered in the yellow zone
      size = self.sm_font.getsize(text)
      t_x = (self.screen_dim[2] - size[0]) // 2
      t_y = 0
      draw.text((t_x,t_y), text, font=self.sm_font, fill="white")

   # Draws the currently selected locatoin as a token offset from the others
   def draw_select(self, draw):
      # Draw token with color dependent on whose turn it is
      if self.turn == 'b':
         x = (self.token_size + 2) * self.select_pos[0] + self.board_dim[0] + 3
         y = (self.token_size + 2) * self.select_pos[1] + self.board_dim[1] + 5
         draw_bitmap(draw, x, y, self.black_token)
      elif self.turn == 'w':
         x = (self.token_size + 2) * self.select_pos[0] + self.board_dim[0] + 3
         y = (self.token_size + 2) * self.select_pos[1] + self.board_dim[1] + 5
         draw_bitmap(draw, x, y, self.white_token)
      
   # Changes turn (needs to check if moves are possible)
   # Returns: Whether there are any more possible moves
   def change_turn(self):
      
      # Toggle the turn
      if self.turn == 'w':
         self.turn = 'b'
      else:
         self.turn = 'w'
      
      # Check if player can move
      if not self.board.can_move(self.turn):
         # Toggle again if player can't
         if self.turn == 'w':
            self.turn = 'b'
         else:
            self.turn = 'w'
            
         # If next player also can't move, the game is over
         return self.board.can_move(self.turn)
      
      return True 
   
   # Updates the board state based on the input
   # control: the user input
   def update_state(self, control):

      # Check if the x_rot has been clicked
      if control['x_clicked']:
         # Turn to menu if so
         self.menu_select = True
         return
      
      # Check if the game is done
      if self.done:
      
         # Check if we're still showing who won
         if self.win_state:
            # Wait for y_rot click to move to "Play again:"
            if control['y_clicked']:
               self.win_state = False
         else:
            # Manage the 'replay' option with y_rot for input
            if control['y'] != 0:
               self.replay_select = not self.replay_select
            if control['y_clicked']:
               if self.replay_select:
                  self.reset()
               else:
                  self.status = -1
                  
      else:
         # Check if it's the AI's turn
         if self.ai and self.ai_color == self.turn:
            
            # Get the order of preference of moves for the AI
            moves = self.brain.decide(self.board.get_state(self.turn))
            
            # Iterate to find the first valid move
            loc = (0,0)
            for loc in moves:
               if self.board.place_tile(loc, self.turn, False):
                  break
            
            # Move the token to that position and click to select
            self.select_pos = [loc[0],loc[1]]
            control['y_clicked'] = True
         
         # Check if y_rot is clicked
         if control['y_clicked']:
            
            # Place tile if location is valid
            if self.board.place_tile(self.select_pos, self.turn):
               # Check if changing turns shows victory
               if not self.change_turn():
                  self.done = True
                  self.results = self.board.winning()
     
         # Update selection positions based on x and y rotations
         self.select_pos[0] += control['x']
         self.select_pos[0] %= 6
         self.select_pos[1] += control['y']
         self.select_pos[1] %= 6

   # Resets the game state (see constructor for variable meanings)
   def reset(self):
      self.select_pos = [4,4]
      self.turn = 'w'
      self.status = None
      self.done = False
      self.results = ('w',-1)
      self.board = Board()
      self.replay_select = True
      self.win_state = True
      self.menu_select = False