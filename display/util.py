# game.py
# author: Andrew Baas
# This file contains the implementation of the Game class. This manages the
# game board, user interaction with it, and the display of that board

# Imports from external dependencies
import os

# Draws a bitmap onto a draw object at a given location
# draw:  The draw image object used to draw on the display
# x:     The x-coordinate of the drawn image
# y:     The y-coordinate of the drawn image
# bmp:   The bitmap array
def draw_bitmap(draw, x, y, bmp):
   # Iterate through each row
   for i in range(len(bmp)):
       line = bmp[i]
       j = 0
       # Iterate through each column
       while line > 0:
           if line & 0x1:
               draw.point((x + i, y + j), "white")
           else:
               draw.point((x + i, y + j), "black")
           line >>= 1
           j += 1

# Returns the contents of the "brains" directory
def get_brains():
   return os.listdir("brains")
