# game.py
# author: Andrew Baas
# This file contains the implementation of the Game class. This manages the
# game board, user interaction with it, and the display of that board

# Imports from external dependencies
import numpy as np
import pickle

# Imports from custom local libraries
from othello.environment import Environment
from display.menu import Menu

# The Train class manages the training of "brains" by allowing the user to
# choose hyperparameters for the training process
class Train():

   # Settings for the training selection menu
   menu_size   = (0, 16, 127, 63)
   items       = ["Net #", "Mutation", "Layers", "Generations"]
   options     = [["4","8","16","32","64","128"],
                  ["0.01","0.05","0.1","0.15","0.20","0.25","0.3","0.4","0.5",
                          "0.6","0.7","0.8","0.9"],
                  ["1","2","4","8","16","32","64"],
                  ["1","2","4","8","16","32","64","128","256","512","1024"]]

   def __init__(self, font):
      
      # Menu settings
      self.font = font
      self.menu = Menu(self.items, self.options, self.font)
      
      # State variables for interaction
      self.results = None        # Stored results from training (to be graphed)
      self.options_screen = True # Whether the options screen is displayed
      self.selected = None       # Stand-in status variable. Currently unused
      
      # Training environment variables
      self.population = 10
      self.generations = 10
      self.mutation = 0.01
      self.layers = 3
      self.cur_gen = -1
      
      # Training environment
      self.environment = None 

   # Renders the training menu or training graph and processes user input
   # draw:     The draw image object used to draw on the display
   # control:  The hardware input
   def render(self, draw, control):
      self.selected = None
      
      # Check if the menu should be displayed
      if self.options_screen:
         
         # Render the menu
         status = self.menu.render(draw, control)
         
         # Process the options if something is selected
         if status != None:
            self.options_screen = False
            selection = self.menu.get_options()
            self.population = int(self.options[0][selection[0]])
            self.mutation = float(self.options[1][selection[1]])
            self.layers = int(self.options[2][selection[2]])
            self.generations = int(self.options[3][selection[3]])
            self.environment = Environment(pop_size=self.population,
                                           mut_rate=self.mutation,
                                           mem_size=self.layers)
            self.results = np.ones(self.generations)
      else:
         # Check if the training is finished
         if self.cur_gen == self.generations:
            #Save the new file and exit
            file_name = "p:" + str(self.population) + \
                        "_l:" + str(self.layers) + \
                        "_g:" + str(self.generations) + \
                        "_m:" + str(self.mutation) + ".brn"
            with open("brains/" + file_name, "wb+") as f:
               pickle.dump(self.environment.population, f)
            return -1

         # Draw the screen if it's the first generation (so we're not stuck on 
         # a menu for the entire first generation)
         if self.cur_gen != -1:
            self.results[self.cur_gen] = self.environment.generation()
         
         # Draw the training plot
         self.draw_plot(draw)
         self.cur_gen += 1
         
      return self.selected

   # Draws the plot of loss rate for each generation
   # draw:     The draw image object used to draw on the display
   def draw_plot(self, draw):

      # Draw a plot only if there is data
      if self.cur_gen >= 0:
         # Scale and make a log graph
         fit = np.log(1 - self.results[0:self.cur_gen + 1])
         fit_max = max(fit)
         fit_min = min(fit)
         if fit_max != fit_min:
            fit_scale = 1 / (fit_max - fit_min)
         else:
            fit_scale = fit_max
         fit = (fit - fit_min) * fit_scale
         print(fit)
         x_scale = (self.menu_size[2] - self.menu_size[0]) / self.generations

         # Plot the graph
         for i in range(self.cur_gen + 1):
            cur_lx = round(i * x_scale)
            cur_rx = round((i + 1) * x_scale)
            cur_y  = self.menu_size[1] + round((1-fit[i]) * (self.menu_size[3] - self.menu_size[1]))
            draw.rectangle([(cur_lx,cur_y),(cur_rx, self.menu_size[3])], fill="white") 
         
      # Plot the current status
      if self.cur_gen >= 0:
         text = "Gen " + str(self.cur_gen + 1) + "; Last: {:.2}".format(1 - self.results[self.cur_gen])
      else:
         text = "Gen " + str(self.cur_gen + 1) + "; Best: N/A"
      
      # Center and render the status
      top_size = self.font.getsize(text)
      top_x = (self.menu_size[2] - top_size[0]) // 2
      draw.text((top_x,0),text, font = self.font, fill="white")