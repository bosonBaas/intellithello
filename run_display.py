# run_display.py
# author: Andrew Baas

# This file runs the program!
from display.display import Display

# Create and run a Display object
d = Display()
d.render()
