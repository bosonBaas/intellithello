# rotary.py
# author: Andrew Baas

# This file contains the implementation of the Rotary class. It acts as the API
# for the rotary encoder input.

# Imports from external dependencies
from RPi import GPIO
from time import sleep
from threading import Thread

# The Rotary class handles direct input from the rotary encoder hardware. It
# allows the creation of parallel threads to listen for input and allows
# access to rotational counters and click states
class Rotary:
   
   # Initializes the Rotary class
   # sw, dt, clk: The index of GPIO pins the respective rotary encoder pins 
   #              are connected to
   def __init__(self, sw, dt, clk):
      
      # Initialize the encoder pins
      self.clk = clk
      self.dt  = dt
      self.sw = sw
      
      # Initialize state information
      self.running = False # Whether the listener is running
      self.counter = 0     # Rotation counter
      self.clicked = False # Whether the encoder has been clicked

      # Initialize the GPIO library
      GPIO.setmode(GPIO.BOARD)
      GPIO.setup(sw, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
      GPIO.setup(clk, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
      GPIO.setup(dt, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
   
   # This function constantly checks for rotations in the rotary encoder   
   def run_rot(self):
      
      # Creates a basis for the clk pin
      clkLastState = GPIO.input(self.clk)
      while self.running:
         
         # Update states of pins
         clkState = GPIO.input(self.clk)
         dtState = GPIO.input(self.dt)
         # Determine directionality of rotation
         if clkState != clkLastState and clkState:
            if dtState != clkState:
               self.counter += 1
            else:
               self.counter -= 1
         # Update the last state
         clkLastState = clkState
         sleep(0.001)

   # This function constantly checks for clicks
   def run_sw(self):
      # Set a basline for the click pin
      swLastState = GPIO.input(self.sw)
      while self.running:
         swState = GPIO.input(self.sw)
         # If the state has changed, update the click
         if swState != swLastState and swState == 0:
            self.clicked = True
         swLastState = swState
         sleep(0.1)

   # This function resets the clicked flag
   def reset_click(self):
      self.clicked = False
   
   # This function manages the parallel threads listening
   # to the hardware
   def listen(self):
      # Make sure it's not already running parallel threads
      if not self.running:
         self.running = True
         self.thread_r = Thread(target=self.run_rot)
         self.thread_r.start()
         self.thread_s = Thread(target=self.run_sw)
         self.thread_s.start()
      else:
         print("Job already running")
         
   # Stops the parallel threads (they need to be recreated to run again)
   def stop(self):
      self.running = False